(define-module (ds-guix fonts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages zip)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages pkg-config))

;http://users.teilar.gr/~g1951d/Aegyptus.zip

(define-public font-symbola
  (package
    (name "font-symbola")
    (version "1.0")
    (source (origin
             (method url-fetch)
             (uri (string-append "http://users.teilar.gr/~g1951d/Symbola.zip"))
             (sha256
              (base32
               "1lfs2j816332ysvpb5ibj2gwpmyqyispqdl7skkshf2gra18hmhd"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let ((unzip      (string-append (assoc-ref %build-inputs
                                                               "unzip")
                                                    "/bin/unzip"))

                         (font-dir (string-append
                                    %output "/share/fonts/truetype"))
                         (conf-dir (string-append
                                    %output "/share/fontconfig/conf.avail"))
                         (doc-dir  (string-append
                                    %output "/share/doc/" ,name "-" ,version)))
                     ;(setenv "PATH" PATH)
                     (system* unzip (assoc-ref %build-inputs "source"))
                     (mkdir-p font-dir)
                     (copy-file "Symbola.ttf" (string-append font-dir "/" "Symbola.ttf"))))))
    (native-inputs `(("source" ,source)
                     ("unzip" ,unzip)))
    (home-page "http://users.teilar.gr/~g1951d/")
    (synopsis "symbol fonts")
    (description "DejaVu provides an expanded version of the Vera font family
aiming for quality and broader Unicode coverage while retaining the original
Vera style.  DejaVu currently works towards conformance with the Multilingual
European Standards (MES-1 and MES-2) for Unicode coverage.  The DejaVu fonts
provide serif, sans and monospaced variants.")
    (license
     (license:x11-style
      "http://dejavu-fonts.org/"))))


(define-public font-aegyptus
  (package
    (name "font-aegyptus")
    (version "1.0")
    (source (origin
             (method url-fetch)
             (uri (string-append "http://users.teilar.gr/~g1951d/Aegyptus.zip"))
             (sha256
              (base32
               "10mr54ja9b169fhqfkrw510jybghrpjx7a8a7m38k5v39ck8wz6v"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let ((unzip      (string-append (assoc-ref %build-inputs
                                                               "unzip")
                                                    "/bin/unzip"))

                         (font-dir (string-append
                                    %output "/share/fonts/truetype"))
                         (conf-dir (string-append
                                    %output "/share/fontconfig/conf.avail"))
                         (doc-dir  (string-append
                                    %output "/share/doc/" ,name "-" ,version)))
                     ;(setenv "PATH" PATH)
                     (system* unzip (assoc-ref %build-inputs "source"))
                     (mkdir-p font-dir)
                     (copy-file "AegyptusB.ttf" (string-append font-dir "/" "AegyptusB.ttf"))
                     (copy-file "AegyptusR.ttf" (string-append font-dir "/" "AegyptusR.ttf"))
                     (copy-file "Gardiner.ttf" (string-append font-dir "/" "Gardiner.ttf"))
                     (copy-file "Nilus.ttf" (string-append font-dir "/" "Nilus.ttf"))
                     (copy-file "AegyptusB_hint.ttf" (string-append font-dir "/" "AegyptusB_hint.ttf"))
                     (copy-file "AegyptusR_hint.ttf" (string-append font-dir "/" "AegyptusR_hint.ttf"))
                     (copy-file "Gardiner_hint.ttf" (string-append font-dir "/" "Gardiner_hint.ttf"))
                     (copy-file "Nilus_hint.ttf" (string-append font-dir "/" "Nilus_hint.ttf"))
                     ))))
    (native-inputs `(("source" ,source)
                     ("unzip" ,unzip)))
    (home-page "http://users.teilar.gr/~g1951d/")
    (synopsis "ehiroglyh fonts")
    (description "DejaVu provides an expanded version of the Vera font family
aiming for quality and broader Unicode coverage while retaining the original
Vera style.  DejaVu currently works towards conformance with the Multilingual
European Standards (MES-1 and MES-2) for Unicode coverage.  The DejaVu fonts
provide serif, sans and monospaced variants.")
    (license
     (license:x11-style
      "http://dejavu-fonts.org/"))))
